import blowfish;

import std.stdio;

int main()
{
    string key = "The quick brown fox jumps over the lazy dog.";
    Blowfish bf;
    bf.setKey(key);
    string text = "This is not a pipe.";
    
    auto encrypted = bf.encrypt(text);
    writefln("%(%x %)", cast(const(ubyte)[])encrypted);
    auto decrypted = bf.decrypt(encrypted);
    writeln(decrypted);
    return 0;
}